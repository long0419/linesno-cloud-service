package com.alinesno.cloud.common.core.exceptions.aop;

import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ServiceExceptionAspect {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ServiceExceptionAspect.class);

}
