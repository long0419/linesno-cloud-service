package com.alinesno.cloud.common.facade.pageable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 用于feign客户端配置
 * 
 * @author LuoAnDong
 * @since 2018年12月13日 下午2:54:36
 * @param <T>
 */
@Deprecated
public class RestPage<T> implements Page<T> {

	private static final Logger log = LoggerFactory.getLogger(RestPage.class) ; 
	
	private List<T> content = new ArrayList<T>() ;
	private int number ;
    private int numberOfElements ;
    private int size ;
    private Sort sort ;
    private long totalElements ;
    private int totalPages ;
    private boolean hasPrevious ; 
    private boolean hasContent ;
    private boolean hasNext ;
    private boolean isFirst ;
    private boolean isLast ;
    private Pageable nextPageable ;
    private Pageable previousPageable ;
    private Iterator<T> iterator ;
	
	private Page<T> delegate ;

	public RestPage(
			@JsonProperty("page") int number,
			@JsonProperty("size") int size) {
		delegate = new PageImpl<>(content, PageRequest.of(number, size), 0);
	}

	public RestPage(Page<T> page) {
		log.debug("===> page:{}" , JSONObject.toJSON(page));
		delegate = new PageImpl<>(page.getContent(), PageRequest.of(page.getNumber(), page.getSize()),page.getTotalElements());
		
		content = page.getContent() ; 
		number = page.getNumber() ;
		numberOfElements = page.getNumberOfElements() ;
		size = page.getSize();
		sort = page.getSort();
		totalElements = page.getTotalElements() ;
		totalPages = page.getTotalPages() ;
		hasContent = page.hasContent() ;
		hasNext = page.hasNext() ;
		isFirst = page.isFirst() ;
		isLast = page.isLast() ;
		hasPrevious = page.hasPrevious() ; 
		nextPageable = page.nextPageable() ;
		previousPageable = page.previousPageable() ;
		iterator = page.iterator() ; 
	
		log.debug("===> number:{} , "
				+ "numberOfElements:{} , "
				+ "size:{} , "
				+ "sort:{} , "
				+ "totalElements:{} , "
				+ "totalPages:{} , "
				+ "hasContent:{} , "
				+ "hasNext:{} , "
				+ "isFirst:{} , "
				+ "isLast:{} , "
				+ "hasPrevious:{} , "
				+ "nextPageable:{} , "
				+ "previousPageable:{} , "
				+ "iterator:{}" , 
				number ,
				numberOfElements ,
				size ,
				sort ,
				totalElements ,
				totalPages ,
				hasContent ,
				hasNext , 
				isFirst ,
				isLast ,
				hasPrevious ,
				nextPageable ,
				previousPageable ,
				iterator );
		
		log.debug("===> delegate:{}" , JSONObject.toJSON(delegate));
	}

	@JsonProperty
	public int getTotalPages() {
		return delegate.getTotalPages() ;
	}

	@JsonProperty
	public long getTotalElements() {
		return delegate.getTotalElements() ;
	}

	@JsonProperty("page")
	public int getNumber() {
		return delegate.getNumber() ;
	}

	@JsonProperty
	public int getSize() {
		return delegate.getSize() ;
	}

	@JsonProperty
	public int getNumberOfElements() {
		return delegate.getNumberOfElements() ;
	}

	@JsonProperty
	public List<T> getContent() {
		return content ; 
	}

	@JsonProperty
	public boolean hasContent() {
		return delegate.hasContent() ;
	}

	@JsonIgnore
	public Sort getSort() {
		return delegate.getSort() ;
	}

	@JsonProperty
	public boolean isFirst() {
		return isFirst ;
	}

	@JsonProperty
	public boolean isLast() {
		return delegate.isLast() ;
	}

	@JsonIgnore
	public boolean hasNext() {
		return delegate.hasNext() ;
	}

	@JsonIgnore
	public boolean hasPrevious() {
		return delegate.hasPrevious() ;
	}

	@JsonIgnore
	public Pageable nextPageable() {
		return delegate.nextPageable() ;
	}

	@JsonIgnore
	public Pageable previousPageable() {
		return delegate.previousPageable() ;
	}

	@JsonIgnore
	public Iterator<T> iterator() {
		return delegate.iterator() ; 
	}

	public <U> Page<U> map(Function<? super T, ? extends U> converter) {
		return delegate.map(converter);
	}

	public boolean isHasPrevious() {
		return hasPrevious;
	}

	public void setHasPrevious(boolean hasPrevious) {
		this.hasPrevious = hasPrevious;
	}

	public boolean isHasContent() {
		return hasContent;
	}

	public void setHasContent(boolean hasContent) {
		this.hasContent = hasContent;
	}

	public boolean isHasNext() {
		return hasNext;
	}

	public void setHasNext(boolean hasNext) {
		this.hasNext = hasNext;
	}

	public Pageable getNextPageable() {
		return nextPageable;
	}

	public void setNextPageable(Pageable nextPageable) {
		this.nextPageable = nextPageable;
	}

	public Pageable getPreviousPageable() {
		return previousPageable;
	}

	public void setPreviousPageable(Pageable previousPageable) {
		this.previousPageable = previousPageable;
	}

	public Iterator<T> getIterator() {
		return iterator;
	}

	public void setIterator(Iterator<T> iterator) {
		this.iterator = iterator;
	}

	public Page<T> getDelegate() {
		return delegate;
	}

	public void setDelegate(Page<T> delegate) {
		this.delegate = delegate;
	}

	public static Logger getLog() {
		return log;
	}

	public void setContent(List<T> content) {
		this.content = content;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public void setNumberOfElements(int numberOfElements) {
		this.numberOfElements = numberOfElements;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void setSort(Sort sort) {
		this.sort = sort;
	}

	public void setTotalElements(long totalElements) {
		this.totalElements = totalElements;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public void setFirst(boolean isFirst) {
		this.isFirst = isFirst;
	}

	public void setLast(boolean isLast) {
		this.isLast = isLast;
	}
	
}