package com.alinesno.cloud.operation.cmdb.common.excel;

import java.io.File;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alinesno.cloud.operation.cmdb.entity.AssertsEntity;
import com.alinesno.cloud.operation.cmdb.entity.ResourceHeadthEntity;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;

public class PoiExcelImportTest {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 

	private String path = "C:\\Users\\lenovo\\Desktop\\04_headthjob.xlsx" ; 

	@Test
	public void testImportBean() {
		
		List<AssertsEntity> list = PoiExcelImport.importBean(path, AssertsEntity.class) ; 
	
		for(AssertsEntity l : list) {
			logger.debug("l:{}" , ReflectionToStringBuilder.toString(l));
		}
		
	}
	
	@Test
    public void test() {
        try {
            ImportParams params = new ImportParams();
            List<ResourceHeadthEntity> result = ExcelImportUtil.importExcel(new File(path),ResourceHeadthEntity.class, params);
            
            for (int i = 0; i < result.size(); i++) {
                System.out.println(ReflectionToStringBuilder.toString(result.get(i)));
            }
            
//            Assert.assertTrue(result.size() == 4);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
