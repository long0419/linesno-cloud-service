package com.alinesno.cloud.platform.stack.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alibaba.fastjson.JSONObject;
import com.alinesno.cloud.operation.cmdb.common.util.DateUtil;
import com.alinesno.cloud.operation.cmdb.entity.DatabaseEntity;
import com.alinesno.cloud.operation.cmdb.repository.DatabaseRepository;

/**
 * 数据库保存
 * @author LuoAnDong
 * @since 2019年3月13日 下午5:01:45
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DatabaseRepositoryTest {

	@Autowired
	private DatabaseRepository databaseRepository ; 
	
	@Test
	public void testSaveAllIterableOfS() {
		
		List<String> arr = new ArrayList<String>() ;
		
		arr.add("1|GZPT_TEST_GZ|GZPT_TEST_GZ_DATA|172.20.200.7|罗安东|20190101|20190630") ; 
		arr.add("2|GZPT_TEST_GZCBS|GZPT_TEST_GZCBS_DATA|172.20.200.7|罗安东|20190101|20190630") ; 
		arr.add("3|GZPT_TEST_GZJZQS|GZPT_TEST_GZJZQS_DATA|172.20.200.7|罗安东|20190101|20190630") ; 
		arr.add("4|GZPT_PRO_GZ|GZPT_PRO_GZ_DATA|172.20.200.7|罗安东|20190101|20190630") ; 
		arr.add("5|PLATFORM|PLATFORM|172.20.200.7|罗安东|20190101|20190630") ; 
		arr.add("6|GZPT_GZ_JZQS|GZPT_GZ_JZQS_DATA|172.20.200.7|罗安东|20190101|20190630") ; 
		arr.add("7|GZPT_GZ_CBS|GZPT_GZ_CBS_DATA|172.20.200.7|罗安东|20190101|20190630") ; 
		arr.add("8|WLJK|WLJK_DATA|172.20.200.7|罗安东|20190101|20190630") ; 
		arr.add("9|WLJK_GONGXIANG|WLJK_GONGXIANG_DATA|172.20.200.7|罗安东|20190101|20190630") ; 
		arr.add("10|GZPT_LOGGER|GZPT_LOGGER_DATA|172.20.200.7|罗安东|20190101|20190630") ; 
		arr.add("11|WLJK20|WLJK20_DATA|172.20.200.7|罗安东|20190101|20190630") ; 
		arr.add("12|MGMT_VIEW|USERS|172.20.200.7|罗安东|20190101|20200630") ; 
		arr.add("13|ETLBACKUP|USERS|172.20.200.7|罗安东|20190101|20200630") ; 
		arr.add("14|DEV01|USERS|172.20.200.7|罗安东|20190101|20200630") ; 
		arr.add("15|SEL01|USERS|172.20.200.7|罗安东|20190101|20200630") ; 
		arr.add("16|GZGJJBAK|USERS|172.20.200.7|罗安东|20190101|20200630") ; 
		arr.add("17|DPBACKUP|USERS|172.20.200.7|罗安东|20190101|20200630") ; 
		arr.add("18|MMD|MMD_DATA|172.20.200.7|罗安东|20190101|20190630") ; 
		arr.add("19|ZHXZ|ZHXZ_DATA_01|172.20.200.7|罗安东|20190101|20190630") ; 
		arr.add("20|ISRP|ISRP_DATA|172.20.200.7|罗安东|20190101|20190630") ; 
		arr.add("21|GZJC|GZJC_DATA|172.20.200.7|严茂祥|20190101|20190630") ; 
		arr.add("22|PLATFORM_TEST|PLATFORM_TEST|172.20.200.7|罗安东|20190101|20200630") ; 
		arr.add("23|GZPT_TEST|GZPT_TEST_DATA|172.20.200.7|罗安东|20190101|20200630") ; 
		arr.add("24|zjjk|zjjk_data|172.20.200.7|吴波|20190101|20190630") ; 
		arr.add("1|GJJYH|GJJYH|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("2|GJJ|GZGJJ|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("3|CBSDB|CBSDB|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("4|TXDB|TXDK_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("5|ZJJG|GZDBZX_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("6|GZJC|GJJ_JC_TEST|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("7|BSGJJ|BSGJJ|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("9|MGMT_VIEW|USERS|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("10|DBBAK|USERS|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("11|BSWSYW|BSWSYW_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("12|BSAMP|BSAMP_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("13|BSLWJS|BSLWJS|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("14|BS01|GZ_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("15|ISO01|GZ_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("16|BSGJJ_1210|BSGJJ_1210|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("17|CZGJJ_1222|CZGJJ_1222|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("18|CZGJJ_1223|CZGJJ_1223|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("19|GB_BSGJJ|GB_BSGJJ|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("20|GXBSGJJ|GXBSGJJ|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("21|GGLWJS|GGLWJS|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("22|GB_BSGJJ01|GB_BSGJJ01|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("23|TXDB2|TXDB_TBS|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("24|GB_BSGJJ02|GB_BSGJJ02|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("25|QYGJJ_0203|QY_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("26|GB_QYGJJ|GB_QYGJJ|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("27|QYGJJ_0416|QYGJJ|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("28|NNWSYW|WSYW_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("29|QDDX|QDDX_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("30|YYXT|YYXT_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("31|NNGJJ|NN_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("32|GZPT_QZGJJ|GZPT_QZGJJ_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("33|NNGJJDS|GZPT_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("34|BSLWJS0504|BSLWJS0504|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("35|PLATFORM|PLATFORM_GZGJJ_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("36|UNITPLATFORM|UNITPLATFORM_GZGJJ_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("37|GB_JMGJJ|GB_JMGJJ|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("38|GB_QYGJJ0607|GB_QYGJJ0607|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("39|GBQYGJJ|GB_QYGJJ0607|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("40|NNGJJCS|NNGJJCS_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("41|ETLBACKUP|USERS|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("42|OGG|OGG|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("43|DEV01|USERS|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("44|BAK|USERS|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("45|GZPT_ZX|GZPT_ZX_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("46|GB_JMGJJ0616|GB_JMGJJ0616|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("47|GB_JMGJJ0710|GB_JMGJJ0710|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("48|NNGJJ_TEST|GJJ_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("49|ESB|GJJ_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("50|BS_WSYW|BS_WSYW_DATA|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("51|GB_CZGJJ|CZGJJ_1222|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("52|MYHIBERNATE|GGLWJS|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("53|JMGJJ_0927|JMGJJ_DATA_0927|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("54|CAPINFO|GGLWJS|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("55|SEL01|USERS|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("56|GZGJJBAK|USERS|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("57|DPBACKUP|USERS|172.20.200.8|罗安东|20190101|20190630") ; 
		arr.add("1|QYGJJ|GDQYGJJ_DATA|172.20.200.14|邱伙荣|20180630|20190101") ; 
		arr.add("2|SGGJJ|GJJ_DATA|172.20.200.14|邱伙荣|20180630|20190101") ; 
		arr.add("3|YFGJJ|GJJ_DATA_YF|172.20.200.14|邱伙荣|20180630|20190101") ; 
		arr.add("4|GJJ_DATA_YFGJJ|GJJ_DATA_YFGJJ|172.20.200.14|邱伙荣|20180630|20190101") ; 
		arr.add("1|BSGJJ_1210|BSGJJ_1210|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("2|GZGJJ|GZ_DATA|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("3|GJJ|GZGJJ|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("4|ESCROW|GZDBZX_DATA|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("5|AMP|AMP_DATA|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("6|GJJYH|GJJYH|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("7|CBSDB|CBSDB|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("8|TXDB|TXDB_DATA|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("9|SGGJJ|SGGJJ|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("10|NNQY|NNQY|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("11|QYWSYW|WSYW_DATA|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("12|QYGJJ|GDQYGJJ_DATA|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("13|QYGJJ0923|GDQYGJJ_DATA|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("14|GZJC|GZJC_DATA|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("15|YYXT|YYXT_DATA|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("16|ISOGJJ|ISOGJJ_DATA|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("17|BSGJJ_0406|BSGJJ_0406|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("18|YFGJJ|YFGJJ_DATA|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("19|QYGJJ_1215|QYGJJ_DATA|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("20|QYGJJ_GB|QYGJJ_GB|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("21|QYGJJ_0127|QY_DATA|172.20.200.17|祁成程|20190101|20190630") ; 
		arr.add("22|TEST_DEPT|TEST_DEPT_DATA|172.20.200.17|祁成程|20190101|20190630") ; 

		List<DatabaseEntity> ms = new ArrayList<DatabaseEntity>() ; 
		for(String s : arr) {
	
			String a[] = s.split("\\|") ; 
			
			DatabaseEntity m = new DatabaseEntity() ; 
			m.setAddTime(new Date());
			m.setDbaLoginName(a[1]); 
			m.setSpaceDesc(a[2]);
			m.setHostIp(a[3]);
			m.setManagerMan(a[4]);
			m.setSendTime(DateUtil.parseDate(a[5]));
			m.setEndTime(DateUtil.parseDate(a[6]));
			
			m.setMasterCode("001");
			
			ms.add(m) ; 
		}
		
		for(DatabaseEntity m : ms) {
			System.out.println(JSONObject.toJSON(m));
		}
		
		databaseRepository.saveAll(ms) ;
	}
	
}
