package com.alinesno.cloud.operation.cmdb.third.headth;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class EchoClientTest {
	
	private static final Logger logger = LoggerFactory.getLogger(EchoClientTest.class) ; 
	
	@Test
	public void testConnect() {
	
		long start = System.currentTimeMillis() ; 
		
		int port = 6379;
		try {
			boolean b = new EchoClient().connect(port, "115.28.233.17");
			logger.debug("is succes:{}" , b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		long end = System.currentTimeMillis() ; 
		logger.debug("time second:{}" , (end-start)/1000);
	}

}
