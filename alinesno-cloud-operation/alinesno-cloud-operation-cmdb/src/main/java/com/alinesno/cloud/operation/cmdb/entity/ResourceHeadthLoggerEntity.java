package com.alinesno.cloud.operation.cmdb.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 健康检查
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午12:20:30
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "cmdb_headth_logger")
public class ResourceHeadthLoggerEntity extends BaseEntity {

	private String headthId; // 检查荐内容
	private Date startheadthTime; // 开始检查时间
	private Date lastHeadthTime; // 最后检查(检查结束)时间
	
	/**
	 * 检查时间 
	 */
	private String errorMessage ; 

	public ResourceHeadthLoggerEntity(Date date, String headthId) {
		this.headthId = headthId ; 
		this.startheadthTime = date  ; 
	}

	public ResourceHeadthLoggerEntity(Date date) {
		this.startheadthTime = date;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getHeadthId() {
		return headthId;
	}

	public void setHeadthId(String headthId) {
		this.headthId = headthId;
	}

	public Date getStartheadthTime() {
		return startheadthTime;
	}

	public void setStartheadthTime(Date startheadthTime) {
		this.startheadthTime = startheadthTime;
	}

	public Date getLastHeadthTime() {
		return lastHeadthTime;
	}

	public void setLastHeadthTime(Date lastHeadthTime) {
		this.lastHeadthTime = lastHeadthTime;
	}

}
