package com.alinesno.cloud.operation.cmdb.third.sms;

/**
 * 民大分布式研发小组短信接口
 * @author LuoAnDong
 * @since 2018年3月3日 上午9:19:49
 */
public interface SmsApi {
 
	/**
	 * 发送短信接口
	 * @param smsRequest
	 * @return 短信返回内容
	 */
	SmsResponse sendSms(SmsRequest smsRequest) ; 
	
}