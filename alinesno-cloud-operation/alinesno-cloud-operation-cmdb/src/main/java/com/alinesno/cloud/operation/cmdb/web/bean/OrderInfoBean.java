package com.alinesno.cloud.operation.cmdb.web.bean;

import com.alinesno.cloud.operation.cmdb.entity.OrderInfoEntity;

@SuppressWarnings("serial")
public class OrderInfoBean extends OrderInfoEntity {

	private String orderId; // 订单id
	private String sendStatus; // 发送状态
	private String sendStatusLabel; // 发送状态文字
	private String infoId; // 订单详情
	private String orderEvaluation; // 订单评价
	private String receiveManagerId; // 配送员
	private String userNickName; // 用户昵称

	// 服务器申请内容
	private String orderCpu; // cpu
	private String orderStorage; // 存储
	private String orderMemory; // 内存
	private String orderNetwork; // 网络
	private String orderSystem; // 操作系统
	private String orderUsername; // 用户名
	private String orderPwd; // 申请密码

	public String getOrderCpu() {
		return orderCpu;
	}

	public void setOrderCpu(String orderCpu) {
		this.orderCpu = orderCpu;
	}

	public String getOrderStorage() {
		return orderStorage;
	}

	public void setOrderStorage(String orderStorage) {
		this.orderStorage = orderStorage;
	}

	public String getOrderMemory() {
		return orderMemory;
	}

	public void setOrderMemory(String orderMemory) {
		this.orderMemory = orderMemory;
	}

	public String getOrderNetwork() {
		return orderNetwork;
	}

	public void setOrderNetwork(String orderNetwork) {
		this.orderNetwork = orderNetwork;
	}

	public String getOrderSystem() {
		return orderSystem;
	}

	public void setOrderSystem(String orderSystem) {
		this.orderSystem = orderSystem;
	}

	public String getOrderUsername() {
		return orderUsername;
	}

	public void setOrderUsername(String orderUsername) {
		this.orderUsername = orderUsername;
	}

	public String getOrderPwd() {
		return orderPwd;
	}

	public void setOrderPwd(String orderPwd) {
		this.orderPwd = orderPwd;
	}

	public String getUserNickName() {
		return userNickName;
	}

	public void setUserNickName(String userNickName) {
		this.userNickName = userNickName;
	}

	public String getSendStatusLabel() {
		return sendStatusLabel;
	}

	public void setSendStatusLabel(String sendStatusLabel) {
		this.sendStatusLabel = sendStatusLabel;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getSendStatus() {
		return sendStatus;
	}

	public void setSendStatus(String sendStatus) {
		this.sendStatus = sendStatus;
	}

	public String getInfoId() {
		return infoId;
	}

	public void setInfoId(String infoId) {
		this.infoId = infoId;
	}

	public String getOrderEvaluation() {
		return orderEvaluation;
	}

	public void setOrderEvaluation(String orderEvaluation) {
		this.orderEvaluation = orderEvaluation;
	}

	public String getReceiveManagerId() {
		return receiveManagerId;
	}

	public void setReceiveManagerId(String receiveManagerId) {
		this.receiveManagerId = receiveManagerId;
	}

}
