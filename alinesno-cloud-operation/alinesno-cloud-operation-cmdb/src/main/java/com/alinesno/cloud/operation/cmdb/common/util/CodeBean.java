package com.alinesno.cloud.operation.cmdb.common.util;
public class CodeBean{
		private long createTime ;  //生成时间 
		private String code ; //验证码 
		
		public CodeBean() {
			
		}
		
		public CodeBean(String code , long createTime) {
			this.code = code ; 
			this.createTime = createTime ; 
		}
		
		public long getCreateTime() {
			return createTime;
		}
		public void setCreateTime(long createTime) {
			this.createTime = createTime;
		}
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		
	}