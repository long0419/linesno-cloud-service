package com.alinesno.cloud.operation.cmdb.common.constants;

/**
 * 业务状态
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午6:41:23
 */
public enum BusinessStatusEnum {

	NORMAL(0, "正常"), FORBIDDEN(1, "禁止");

	private int code;
	private String text;

	BusinessStatusEnum(int code, String text) {
		this.code = code;
		this.text = text;
	}

	public static BusinessStatusEnum getStatus(int code) {
		if (1 == code) {
			return BusinessStatusEnum.NORMAL;
		} else if (0 == code) {
			return BusinessStatusEnum.FORBIDDEN;
		}
		return NORMAL;
	}

	public String getText() {
		return this.text;
	}

	public int getCode() {
		return this.code;
	}
}