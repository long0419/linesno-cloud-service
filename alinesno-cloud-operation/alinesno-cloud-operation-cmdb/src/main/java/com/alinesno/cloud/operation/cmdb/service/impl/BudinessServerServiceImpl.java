package com.alinesno.cloud.operation.cmdb.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.operation.cmdb.common.constants.BusinessStatusEnum;
import com.alinesno.cloud.operation.cmdb.entity.ServerEntity;
import com.alinesno.cloud.operation.cmdb.repository.BudinessServerRepository;
import com.alinesno.cloud.operation.cmdb.service.BudinessServerService;

/**
 * 业务服务
 * 
 * @author LuoAnDong
 * @since 2018年10月15日 上午7:45:00
 */
@Service
public class BudinessServerServiceImpl implements BudinessServerService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String DEFAULT_SCHOOL_CODE = "10608" ; 
	
	@Autowired
	private BudinessServerRepository budinessServerRepository ; 
	
	@Override
	public Iterable<ServerEntity> findByMasterCode(String masterCode) {
	
		logger.debug("school code = {}" , masterCode);
		if(StringUtils.isBlank(masterCode)) {
			masterCode = DEFAULT_SCHOOL_CODE ; //默认为广西民大 
		}
		Iterable<ServerEntity> list = budinessServerRepository.findAllByMasterCodeAndHasStatus(masterCode , BusinessStatusEnum.NORMAL.getCode() , Sort.by(Direction.DESC, "serverOrder")) ; 
		
		return list ; 
	}

}
