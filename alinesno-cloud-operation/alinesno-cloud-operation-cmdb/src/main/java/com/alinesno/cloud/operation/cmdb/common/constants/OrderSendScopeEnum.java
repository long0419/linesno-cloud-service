package com.alinesno.cloud.operation.cmdb.common.constants;

/**
 * 订单状态
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午6:41:23
 */
public enum OrderSendScopeEnum {

	ALL("all" , "全部用户"), // 全部用户 
	RUNMAN("runman" , "用户"); // 用户

	public String code;
	public String text ;
	
	OrderSendScopeEnum(String code , String text) {
		this.code = code;
		this.text = text ; 
	}
	
	public static OrderSendScopeEnum getStatus(String code) {
		if("all".equals(code)) {
			return ALL ; 
		}else if("runman".equals(code)) {
			return RUNMAN ; 
		}
		return RUNMAN ; 
	}
	
	public String getText() {
		return this.text ; 
	}

	public String getCode() {
		return this.code;
	}
}