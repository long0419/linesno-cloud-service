package com.alinesno.cloud.operation.cmdb.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.operation.cmdb.entity.WechatMessageEntity;
import com.alinesno.cloud.operation.cmdb.repository.WechatMessageRepository;
import com.alinesno.cloud.operation.cmdb.service.WechatMessageService;

@Service
public class WechatMessageServiceImpl implements WechatMessageService {

	@Autowired
	private WechatMessageRepository wechatMessageRepository ; 

	@Async("processExecutor")
	@Override
	public void save(WechatMessageEntity wechatMessageEntity) {
		wechatMessageEntity.setAddTime(new Date());
		wechatMessageRepository.save(wechatMessageEntity) ; 
	}

}
