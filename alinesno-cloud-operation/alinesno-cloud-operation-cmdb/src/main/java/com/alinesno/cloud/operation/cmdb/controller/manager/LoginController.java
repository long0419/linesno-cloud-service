package com.alinesno.cloud.operation.cmdb.controller.manager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.operation.cmdb.common.constants.RunConstants;
import com.alinesno.cloud.operation.cmdb.common.util.DateUtil;
import com.alinesno.cloud.operation.cmdb.controller.BaseController;
import com.alinesno.cloud.operation.cmdb.entity.AccountEntity;
import com.alinesno.cloud.operation.cmdb.repository.AccountRepository;
import com.alinesno.cloud.operation.cmdb.web.bean.AccountBean;
import com.alinesno.cloud.operation.cmdb.web.bean.ResultGenerator;

/**
 * 登陆控制层
 * @author LuoAnDong
 * @since 2018年8月17日 上午7:48:06
 */
@Controller
@RequestMapping("/manager/")
public class LoginController extends BaseController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AccountRepository accountRepository;

	/**
	 * 登陆面板
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/login")
	public String login(Model model) {
		return WX_MANAGER + "login";
	}

	/**
	 * 登陆验证
	 * 
	 * @return
	 */
	@ResponseBody
	@PostMapping("/validate")
	public Object loginValidate(String loginName, String password , HttpSession session , HttpServletRequest request) {
		logger.debug("login name = {} , 请求参数:{}", loginName , request.getAttributeNames());
		String realIp = request.getRemoteHost() ; 
		Object realIPObj = request.getAttribute("X-Real-Ip") ; 
		if(realIPObj != null && StringUtils.isNoneBlank(realIPObj+"")) {
			realIp = (String) realIPObj ; 
		}

		if (StringUtils.isBlank(loginName)) {
			return ResultGenerator.genFailMessage("登陆名不能为空.");
		}

		if (StringUtils.isBlank(password)) {
			return ResultGenerator.genFailMessage("密码不能为空.");
		}

		if (StringUtils.isNoneBlank(loginName)) {
			AccountEntity account = accountRepository.findByLoginName(loginName);
			if (account != null) {
//				if (account.getPassword().equals(DigestUtils.md5Hex(password+account.getSalt()))) { // .md5Crypt(account.getPassword().getBytes()))) {
					
					account.setLastLoginTime(DateUtil.getFullChinesePatternNow());
					account.setLastLoginIp(realIp) ; 
					accountRepository.save(account) ; 
					
					AccountBean ab = new AccountBean() ; 
					BeanUtils.copyProperties(account, ab); 
					
					session.setAttribute(RunConstants.CURRENT_MANAGER, ab); 
					return ResultGenerator.genSuccessMessage("登陆成功.");
//				}
			}
		}

		return ResultGenerator.genFailMessage("登陆失败.");
	}

	/**
	 * 退出面板
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/logout")
	public String logout(Model model , HttpSession session) {
		session.removeAttribute(RunConstants.CURRENT_MANAGER);
		return redirect("/"+WX_MANAGER + "login");
	}

}
