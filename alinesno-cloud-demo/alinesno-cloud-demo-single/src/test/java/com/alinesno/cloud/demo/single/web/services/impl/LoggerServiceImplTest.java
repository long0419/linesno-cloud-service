package com.alinesno.cloud.demo.single.web.services.impl;

import static org.junit.Assert.*;

import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.common.core.junit.JUnitBase;
import com.alinesno.cloud.demo.single.web.entity.LoggerEntity;
import com.alinesno.cloud.demo.single.web.services.LoggerService;

/**
 * 日志操作单元测试
 * @author LuoAnDong
 * @since 2018年11月20日 下午8:22:33
 */
public class LoggerServiceImplTest extends JUnitBase {

	@Autowired
	private LoggerService loggerService ; 
	
	@Test
	public void testFindAll() {
		log.debug("loggerService = {}", loggerService);
		loggerService.findAll() ; 
	}

	@Test
	public void testFindAllSort() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllById() {
		fail("Not yet implemented");
	}

	@Test
	public void testSaveAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testFlush() {
		fail("Not yet implemented");
	}

	@Test
	public void testSaveAndFlush() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteInBatch() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAllInBatch() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetOne() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfSSort() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllPageable() {
		fail("Not yet implemented");
	}

	@Test
	public void testSave() {
		LoggerEntity entity = new LoggerEntity() ; 
		entity.setAddTime(new Date());
		entity.setRecordMsg("测试保存信息" + System.currentTimeMillis());
		entity = loggerService.save(entity) ; 
		
		log.debug("entity = {}" , ToStringBuilder.reflectionToString(entity));
	}

	@Test
	public void testFindById() {
		fail("Not yet implemented");
	}

	@Test
	public void testExistsById() {
		fail("Not yet implemented");
	}

	@Test
	public void testCount() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteById() {
		fail("Not yet implemented");
	}

	@Test
	public void testDelete() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAllIterableOfQextendsEntity() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteAll() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindOneExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllExampleOfSPageable() {
		fail("Not yet implemented");
	}

	@Test
	public void testCountExampleOfS() {
		fail("Not yet implemented");
	}

	@Test
	public void testExists() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindOneSpecificationOfEntity() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfEntity() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfEntityPageable() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindAllSpecificationOfEntitySort() {
		fail("Not yet implemented");
	}

	@Test
	public void testCountSpecificationOfEntity() {
		fail("Not yet implemented");
	}

}
