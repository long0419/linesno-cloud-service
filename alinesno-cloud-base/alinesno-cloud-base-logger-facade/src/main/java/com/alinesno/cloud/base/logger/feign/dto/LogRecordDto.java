package com.alinesno.cloud.base.logger.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
@SuppressWarnings("serial")
public class LogRecordDto extends BaseDto {

    /**
     * 记录时间
     */
	private String recordMsg;
	
    /**
     * 记录渠道
     */
	private String recordChannel;
	
    /**
     * 日志参数
     */
	private String recordParams;
	
    /**
     * 用户名称
     */
	private String recordUser;
	
    /**
     * 日志用户名
     */
	private String recordUserName;
	
    /**
     * 日志记录类型
     */
	private String recordType;
	


	public String getRecordMsg() {
		return recordMsg;
	}

	public void setRecordMsg(String recordMsg) {
		this.recordMsg = recordMsg;
	}

	public String getRecordChannel() {
		return recordChannel;
	}

	public void setRecordChannel(String recordChannel) {
		this.recordChannel = recordChannel;
	}

	public String getRecordParams() {
		return recordParams;
	}

	public void setRecordParams(String recordParams) {
		this.recordParams = recordParams;
	}

	public String getRecordUser() {
		return recordUser;
	}

	public void setRecordUser(String recordUser) {
		this.recordUser = recordUser;
	}

	public String getRecordUserName() {
		return recordUserName;
	}

	public void setRecordUserName(String recordUserName) {
		this.recordUserName = recordUserName;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

}
