package com.alinesno.cloud.base.notice.entity;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Entity;
import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-19 22:02:34
 */
@Entity
@Table(name="sms_template")
public class SmsTemplateEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 模板内容
     */
	@Column(name="template_content")
	private String templateContent;
    /**
     * 模板描述
     */
	@Column(name="template_desc")
	private String templateDesc;
    /**
     * 模板类型
     */
	@Column(name="template_type")
	private String templateType;
    /**
     * 模板代码
     */
	@Column(name="template_code")
	private String templateCode;


	public String getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

	public String getTemplateDesc() {
		return templateDesc;
	}

	public void setTemplateDesc(String templateDesc) {
		this.templateDesc = templateDesc;
	}

	public String getTemplateType() {
		return templateType;
	}

	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}


	@Override
	public String toString() {
		return "SmsTemplateEntity{" +
			"templateContent=" + templateContent +
			", templateDesc=" + templateDesc +
			", templateType=" + templateType +
			", templateCode=" + templateCode +
			"}";
	}
}
