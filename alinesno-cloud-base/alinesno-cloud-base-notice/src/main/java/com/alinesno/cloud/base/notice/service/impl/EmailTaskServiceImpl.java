package com.alinesno.cloud.base.notice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.notice.entity.EmailTaskEntity;
import com.alinesno.cloud.base.notice.repository.EmailTaskRepository;
import com.alinesno.cloud.base.notice.service.IEmailTaskService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@Service
public class EmailTaskServiceImpl extends IBaseServiceImpl<EmailTaskRepository, EmailTaskEntity, String> implements IEmailTaskService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(EmailTaskServiceImpl.class);

}
