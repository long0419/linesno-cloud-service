package com.alinesno.cloud.base.boot.service.impl;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.base.boot.service.IManagerAccountService;
import com.alinesno.cloud.common.core.junit.JUnitBase;

/**
 * 管理员账户用户服务
 * @author LuoAnDong
 * @sine 2019年4月5日 下午12:47:43
 */
public class ManagerAccountServiceImplTest extends JUnitBase {

	@Autowired
	private IManagerAccountService managerAccountService ; 

	@Test
	public void testSave() {
		ManagerAccountEntity entity = new ManagerAccountEntity() ; 
	
		entity.setAccountStatus("1");
		entity.setLastLoginIp("127.0.0.1");
		entity.setLastLoginTime("2018-12-12 12:12:12");
		entity.setLoginName("admin@gmail.com") ; 
		entity.setPassword("admin");
		entity.setSalt("2312");
		entity.setName("超级管理员");
		entity.setRolePower("9");
		
		managerAccountService.save(entity) ; 
	}
	
}
