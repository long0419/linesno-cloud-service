package com.alinesno.cloud.base.boot.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.boot.entity.ContentNoticeEntity;
import com.alinesno.cloud.base.boot.repository.ContentNoticeRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-04 14:20:07
 */
@NoRepositoryBean
public interface IContentNoticeService extends IBaseService<ContentNoticeRepository, ContentNoticeEntity, String> {

}
