package com.alinesno.cloud.base.boot.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Entity
@Table(name="content_links")
public class ContentLinksEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 内容链接
     */
	@Column(name="link_url")
	private String linkUrl;
    /**
     * 内容链接名称
     */
	@Column(name="link_name")
	private String linkName;
    /**
     * 链接图片
     */
	@Column(name="link_image")
	private String linkImage;
    /**
     * 链接打开目标
     */
	@Column(name="link_target")
	private String linkTarget;
    /**
     * 链接描述
     */
	@Column(name="link_description")
	private String linkDescription;
    /**
     * 链接是否可见
     */
	@Column(name="link_visible")
	private String linkVisible;
    /**
     * 链接作者
     */
	@Column(name="link_owner")
	private Long linkOwner;
    /**
     * 链接评分
     */
	@Column(name="link_rating")
	private Integer linkRating;
    /**
     * 链接更新时间 
     */
	@Column(name="link_updated")
	private Date linkUpdated;
    /**
     * 标签
     */
	@Column(name="link_notes")
	private String linkNotes;


	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

	public String getLinkImage() {
		return linkImage;
	}

	public void setLinkImage(String linkImage) {
		this.linkImage = linkImage;
	}

	public String getLinkTarget() {
		return linkTarget;
	}

	public void setLinkTarget(String linkTarget) {
		this.linkTarget = linkTarget;
	}

	public String getLinkDescription() {
		return linkDescription;
	}

	public void setLinkDescription(String linkDescription) {
		this.linkDescription = linkDescription;
	}

	public String getLinkVisible() {
		return linkVisible;
	}

	public void setLinkVisible(String linkVisible) {
		this.linkVisible = linkVisible;
	}

	public Long getLinkOwner() {
		return linkOwner;
	}

	public void setLinkOwner(Long linkOwner) {
		this.linkOwner = linkOwner;
	}

	public Integer getLinkRating() {
		return linkRating;
	}

	public void setLinkRating(Integer linkRating) {
		this.linkRating = linkRating;
	}

	public Date getLinkUpdated() {
		return linkUpdated;
	}

	public void setLinkUpdated(Date linkUpdated) {
		this.linkUpdated = linkUpdated;
	}

	public String getLinkNotes() {
		return linkNotes;
	}

	public void setLinkNotes(String linkNotes) {
		this.linkNotes = linkNotes;
	}


	@Override
	public String toString() {
		return "ContentLinksEntity{" +
			"linkUrl=" + linkUrl +
			", linkName=" + linkName +
			", linkImage=" + linkImage +
			", linkTarget=" + linkTarget +
			", linkDescription=" + linkDescription +
			", linkVisible=" + linkVisible +
			", linkOwner=" + linkOwner +
			", linkRating=" + linkRating +
			", linkUpdated=" + linkUpdated +
			", linkNotes=" + linkNotes +
			"}";
	}
}
