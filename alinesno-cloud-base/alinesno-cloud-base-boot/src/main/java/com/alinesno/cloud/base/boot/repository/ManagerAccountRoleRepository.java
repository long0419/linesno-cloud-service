package com.alinesno.cloud.base.boot.repository;

import java.util.List;

import com.alinesno.cloud.base.boot.entity.ManagerAccountRoleEntity;
import com.alinesno.cloud.common.core.orm.repository.IBaseJpaRepository;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-04-08 08:14:52
 */
public interface ManagerAccountRoleRepository extends IBaseJpaRepository<ManagerAccountRoleEntity, String> {

	List<ManagerAccountRoleEntity> findAllByAccountId(String accountId);

	void deleteByAccountId(String accountId);

}
