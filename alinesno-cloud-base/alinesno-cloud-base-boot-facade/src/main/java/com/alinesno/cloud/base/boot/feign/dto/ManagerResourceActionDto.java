package com.alinesno.cloud.base.boot.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:00:40
 */
@SuppressWarnings("serial")
public class ManagerResourceActionDto extends BaseDto {

    /**
     * 操作名称
     */
	private String resourceActionName;
	
    /**
     * 操作方法名称
     */
	private String resourceActionMethod;
	
    /**
     * 操作图标
     */
	private String resourceActionIcon;
	
    /**
     * 操作状态(1正常/0非法)
     */
	private Boolean resourceActionStatus;
	
    /**
     * 所属资源
     */
	private String resourceId;
	
    /**
     * 排列顺序
     */
	private Integer resourceActionOrder;
	


	public String getResourceActionName() {
		return resourceActionName;
	}

	public void setResourceActionName(String resourceActionName) {
		this.resourceActionName = resourceActionName;
	}

	public String getResourceActionMethod() {
		return resourceActionMethod;
	}

	public void setResourceActionMethod(String resourceActionMethod) {
		this.resourceActionMethod = resourceActionMethod;
	}

	public String getResourceActionIcon() {
		return resourceActionIcon;
	}

	public void setResourceActionIcon(String resourceActionIcon) {
		this.resourceActionIcon = resourceActionIcon;
	}

	public Boolean isResourceActionStatus() {
		return resourceActionStatus;
	}

	public void setResourceActionStatus(Boolean resourceActionStatus) {
		this.resourceActionStatus = resourceActionStatus;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public Integer getResourceActionOrder() {
		return resourceActionOrder;
	}

	public void setResourceActionOrder(Integer resourceActionOrder) {
		this.resourceActionOrder = resourceActionOrder;
	}

}
