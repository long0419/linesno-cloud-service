package com.alinesno.cloud.compoment.code.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 项目sql脚本
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Entity
@Table(name="project_sql")
public class ProjectSqlEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 项目编码
     */
	private String projectCode;
    /**
     * tsql编码
     */
	private String code;
    /**
     * sql脚本
     */
	private String tsql;
    /**
     * 状态：停用[Disenable]，启用[Enable]
     */
	private String state;


	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTsql() {
		return tsql;
	}

	public void setTsql(String tsql) {
		this.tsql = tsql;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}


	@Override
	public String toString() {
		return "ProjectSqlEntity{" +
			"projectCode=" + projectCode +
			", code=" + code +
			", tsql=" + tsql +
			", state=" + state +
			"}";
	}
}
