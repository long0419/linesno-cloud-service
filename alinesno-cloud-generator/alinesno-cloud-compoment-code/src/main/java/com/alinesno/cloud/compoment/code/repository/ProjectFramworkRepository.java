package com.alinesno.cloud.compoment.code.repository;

import com.alinesno.cloud.common.core.orm.repository.IBaseJpaRepository;
import com.alinesno.cloud.compoment.code.entity.ProjectFramworkEntity;

/**
 * <p>
  * 项目应用技术 持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
public interface ProjectFramworkRepository extends IBaseJpaRepository<ProjectFramworkEntity, String> {

}
