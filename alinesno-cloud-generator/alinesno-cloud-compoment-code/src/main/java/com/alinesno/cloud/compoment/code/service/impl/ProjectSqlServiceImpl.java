package com.alinesno.cloud.compoment.code.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.compoment.code.entity.ProjectSqlEntity;
import com.alinesno.cloud.compoment.code.repository.ProjectSqlRepository;
import com.alinesno.cloud.compoment.code.service.IProjectSqlService;

/**
 * <p> 项目sql脚本 服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Service
public class ProjectSqlServiceImpl extends IBaseServiceImpl<ProjectSqlRepository, ProjectSqlEntity, String> implements IProjectSqlService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ProjectSqlServiceImpl.class);

}
