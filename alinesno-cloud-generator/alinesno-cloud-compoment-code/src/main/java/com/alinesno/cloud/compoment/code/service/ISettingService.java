package com.alinesno.cloud.compoment.code.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.compoment.code.entity.SettingEntity;
import com.alinesno.cloud.compoment.code.repository.SettingRepository;

/**
 * <p> 设置 服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@NoRepositoryBean
public interface ISettingService extends IBaseService<SettingRepository, SettingEntity, String> {

}
