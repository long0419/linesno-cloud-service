package com.alinesno.cloud.compoment.code.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.compoment.code.entity.ProjectFramworkEntity;
import com.alinesno.cloud.compoment.code.repository.ProjectFramworkRepository;
import com.alinesno.cloud.compoment.code.service.IProjectFramworkService;

/**
 * <p> 项目应用技术 服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Service
public class ProjectFramworkServiceImpl extends IBaseServiceImpl<ProjectFramworkRepository, ProjectFramworkEntity, String> implements IProjectFramworkService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ProjectFramworkServiceImpl.class);

}
