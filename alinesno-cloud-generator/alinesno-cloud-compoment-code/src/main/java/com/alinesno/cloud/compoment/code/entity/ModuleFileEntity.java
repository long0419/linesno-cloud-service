package com.alinesno.cloud.compoment.code.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 模块文件信息
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Entity
@Table(name="module_file")
public class ModuleFileEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 模块编码
     */
	private String moudleCode;
    /**
     * 文件路径
     */
	private String path;


	public String getMoudleCode() {
		return moudleCode;
	}

	public void setMoudleCode(String moudleCode) {
		this.moudleCode = moudleCode;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}


	@Override
	public String toString() {
		return "ModuleFileEntity{" +
			"moudleCode=" + moudleCode +
			", path=" + path +
			"}";
	}
}
