package com.alinesno.cloud.compoment.code.devops.tools;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.Job;
import com.offbytwo.jenkins.model.JobWithDetails;

public class JenkinsToolsTest {

	private static final Logger log = LoggerFactory.getLogger(JenkinsToolsTest.class) ; 
	
	/**
	 * 测试连接jenkins
	 * @throws URISyntaxException 
	 * @throws IOException 
	 */
	@SuppressWarnings("resource")
	@Test
	public void test() throws URISyntaxException, IOException {
		JenkinsServer jenkins = new JenkinsServer(new URI("http://jenkins.linesno.com/"), "", "$%^") ;
		Map<String, Job> jobs = jenkins.getJobs() ; 
		
		for(String job : jobs.keySet()) {
			JobWithDetails j = jobs.get(job).details() ; // (jobs.get(job)).get("My Job").details() ; 
			log.debug("job:{} , detail:{}" , job , j.getLastSuccessfulBuild());
		}
	}

}
