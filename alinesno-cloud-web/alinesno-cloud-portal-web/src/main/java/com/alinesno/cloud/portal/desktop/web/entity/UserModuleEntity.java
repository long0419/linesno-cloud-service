package com.alinesno.cloud.portal.desktop.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-21 19:10:44
 */
@Entity
@Table(name="user_module")
public class UserModuleEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 模块名称
     */
	@Column(name="module_name")
	private String moduleName;
    /**
     * 模块描述
     */
	@Column(name="module_desc")
	private String moduleDesc;
	
    /**
     * 模块图标
     */
	@Column(name="module_logo")
	private String moduleLogo; 

	/**
	 * 模块路径
	 */
	@Column(name="module_path")
	private String modulePath ; 
	
	 /**
     * 链接打开状态
     */
	@Column(name="open_target")
	private String openTarget ;
	
    /**
     * 所属用户
     */
	private String uid;
    /**
     * 常用模板
     */
	@Column(name="often_module")
	private String oftenModule;
    /**
     * 点击次数
     */
	@Column(name="click_count")
	private Integer clickCount = 0 ;
    /**
     * 模块主键
     */
	private String mid;

	public String getModulePath() {
		return modulePath;
	}

	public String getOpenTarget() {
		return openTarget;
	}

	public void setOpenTarget(String openTarget) {
		this.openTarget = openTarget;
	}

	public void setModulePath(String modulePath) {
		this.modulePath = modulePath;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModuleLogo() {
		return moduleLogo;
	}

	public void setModuleLogo(String moduleLogo) {
		this.moduleLogo = moduleLogo;
	}

	public String getModuleDesc() {
		return moduleDesc;
	}

	public void setModuleDesc(String moduleDesc) {
		this.moduleDesc = moduleDesc;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getOftenModule() {
		return oftenModule;
	}

	public void setOftenModule(String oftenModule) {
		this.oftenModule = oftenModule;
	}

	public Integer getClickCount() {
		return clickCount;
	}

	public void setClickCount(Integer clickCount) {
		this.clickCount = clickCount;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}


	@Override
	public String toString() {
		return "UserModuleEntity{" +
			"moduleName=" + moduleName +
			", moduleDesc=" + moduleDesc +
			", uid=" + uid +
			", oftenModule=" + oftenModule +
			", clickCount=" + clickCount +
			", mid=" + mid +
			"}";
	}
}
