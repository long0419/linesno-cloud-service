package com.alinesno.cloud.portal.desktop.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
@Entity
@Table(name="menus")
public class MenusEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 菜单名称
     */
	@Column(name="menus_name")
	private String menusName;
    /**
     * 菜单图标
     */
	@Column(name="menus_icons")
	private String menusIcons;
    /**
     * 菜单链接
     */
	@Column(name="menus_link")
	private String menusLink;
    /**
     * 打开目标
     */
	@Column(name="open_target")
	private String openTarget;
    /**
     * 是否主导航
     */
	@Column(name="has_nav")
	private String hasNav;
    /**
     * 排序顺序
     */
	@Column(name="menus_sort")
	private Integer menusSort;
    /**
     * 所属类型
     */
	@Column(name="type_id")
	private String typeId;
    /**
     * 菜单类型规划状态
     */
	@Column(name="menus_design_status")
	private String menusDesignStatus;
    /**
     * 父类菜单Id
     */
	@Column(name="parent_menus_id")
	private String parentMenusId;


	public String getMenusName() {
		return menusName;
	}

	public void setMenusName(String menusName) {
		this.menusName = menusName;
	}

	public String getMenusIcons() {
		return menusIcons;
	}

	public void setMenusIcons(String menusIcons) {
		this.menusIcons = menusIcons;
	}

	public String getMenusLink() {
		return menusLink;
	}

	public void setMenusLink(String menusLink) {
		this.menusLink = menusLink;
	}

	public String getOpenTarget() {
		return openTarget;
	}

	public void setOpenTarget(String openTarget) {
		this.openTarget = openTarget;
	}

	public String getHasNav() {
		return hasNav;
	}

	public void setHasNav(String hasNav) {
		this.hasNav = hasNav;
	}

	public Integer getMenusSort() {
		return menusSort;
	}

	public void setMenusSort(Integer menusSort) {
		this.menusSort = menusSort;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getMenusDesignStatus() {
		return menusDesignStatus;
	}

	public void setMenusDesignStatus(String menusDesignStatus) {
		this.menusDesignStatus = menusDesignStatus;
	}

	public String getParentMenusId() {
		return parentMenusId;
	}

	public void setParentMenusId(String parentMenusId) {
		this.parentMenusId = parentMenusId;
	}


	@Override
	public String toString() {
		return "MenusEntity{" +
			"menusName=" + menusName +
			", menusIcons=" + menusIcons +
			", menusLink=" + menusLink +
			", openTarget=" + openTarget +
			", hasNav=" + hasNav +
			", menusSort=" + menusSort +
			", typeId=" + typeId +
			", menusDesignStatus=" + menusDesignStatus +
			", parentMenusId=" + parentMenusId +
			"}";
	}
}
