package com.alinesno.cloud.portal.desktop.web.bean;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class MenusBean implements Serializable {

	 /**
     * 菜单名称
     */
	private String menusName;
	
    /**
     * 菜单图标
     */
	private String menusIcons;
	
    /**
     * 菜单链接
     */
	private String menusLink;
	
    /**
     * 打开目标
     */
	private String openTarget;
	
    /**
     * 是否主导航
     */
	private String hasNav;
	
    /**
     * 排序顺序
     */
	private Integer menusSort;
	
    /**
     * 所属类型
     */
	private String typeId;
	
    /**
     * 菜单类型规划状态
     */
	private String menusDesignStatus;
	
    /**
     * 父类菜单Id
     */
	private String parentMenusId;

	/**
	 * 是否有模块 
	 */
	private boolean hasModule ; 
	
	/**
	 * 模块列表
	 */
	private List<ModuleBean> modules ; 

	/**
	 * 是否有子模块
	 */
	private boolean hasSubItem ;
	
	/**
	 * 子菜单 
	 */
	private List<MenusBean>  subItems; 

	public List<ModuleBean> getModules() {
		return modules;
	}

	public void setModules(List<ModuleBean> modules) {
		this.modules = modules;
	}

	public List<MenusBean> getSubItems() {
		return subItems;
	}

	public void setSubItems(List<MenusBean> subItems) {
		this.subItems = subItems;
	}

	public String getMenusName() {
		return menusName;
	}

	public void setMenusName(String menusName) {
		this.menusName = menusName;
	}

	public String getMenusIcons() {
		return menusIcons;
	}

	public void setMenusIcons(String menusIcons) {
		this.menusIcons = menusIcons;
	}

	public String getMenusLink() {
		return menusLink;
	}

	public void setMenusLink(String menusLink) {
		this.menusLink = menusLink;
	}

	public String getOpenTarget() {
		return openTarget;
	}

	public void setOpenTarget(String openTarget) {
		this.openTarget = openTarget;
	}

	public String getHasNav() {
		return hasNav;
	}

	public void setHasNav(String hasNav) {
		this.hasNav = hasNav;
	}

	public Integer getMenusSort() {
		return menusSort;
	}

	public void setMenusSort(Integer menusSort) {
		this.menusSort = menusSort;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getMenusDesignStatus() {
		return menusDesignStatus;
	}

	public void setMenusDesignStatus(String menusDesignStatus) {
		this.menusDesignStatus = menusDesignStatus;
	}

	public String getParentMenusId() {
		return parentMenusId;
	}

	public void setParentMenusId(String parentMenusId) {
		this.parentMenusId = parentMenusId;
	}

	public boolean isHasModule() {
		return hasModule;
	}

	public void setHasModule(boolean hasModule) {
		this.hasModule = hasModule;
	}

	public boolean isHasSubItem() {
		return hasSubItem;
	}

	public void setHasSubItem(boolean hasSubItem) {
		this.hasSubItem = hasSubItem;
	} 
	
}
